<!DOCTYPE html>
<?php
    class Message{
        protected $text ="A simple message";
        public static $count = 0;
        public function show(){
            echo "$this->text";
        }
        public function change(){
            $this->text="changed";
        }
        function _construct($text = ""){
            $count = $count++;
            if($text != ""){
                $this->text = $text;
            }
        }
    }
    class coloredMessage extends Message{
        protected $color = 'red';
        public function __set($property, $value){
            if($property == 'color'){
                $color = array('red','yellow','green');
                if(in_array($value,$color)){
                  $this->color=$value      
                }
            }
        }
        public function show(){
        echo "<p style = 'color:$this->color'>$this->text</p>";
        }
    }
    class redMessage extends Message {
        public function show(){
            echo "<p style = 'color:red'>$this->text</p>";
        }
    }
    class Htmlpage{
        private $title = "example Title";
        private $body = "examle Body";
        function __construct($text = "" ,$body = ""){
                if($text != ""){
                    $this->title = $text;
                }
                if($body != ""){
                    $this->body = $body;
                }
        }
        public function view(){
            echo "<html>
                 <head>
                       <title>$this->title</title>
                 </head>
                <body>$this->body
                </body>
                 </html>";
        }
    }
?>